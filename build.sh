set -Ceu

yarn rimraf dist public

parcel build src/main.js src/main.debug.js src/style.scss --public-url=/gitlab-ui-20201107/ --no-cache --no-source-maps
cp src/index.html dist/index.html
