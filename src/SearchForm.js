import { stringify } from './compact2'
import GluiForm from './GluiForm'
import { fixTokens } from './fixTokens'

export const SearchForm = {
	name: 'SearchForm',
	props: {
		tokens: {
			type: Object,
			required: true,
		},
		value: {
			type: Array,
			required: true,
		},
	},
	data() {
		return {
			currentValue: this.value,
		}
	},
	computed: {
		availableTokens() {
			return this.tokens
		},
	},
	methods: {
		push(q) {
			location.search = stringify(q, fixTokens(this.availableTokens))
		},
	},
	render(h) {
		return h(GluiForm, {
			attrs: { tokens: this.availableTokens },
			on: { submit: this.push },
			model: {
				value: this.currentValue,
				callback: l => {
					this.currentValue = l
				},
				expression: 'value',
			},
		})
	},
}

export default SearchForm
