const main = document.querySelector('main')
const div = document.createElement('div')
div.setAttribute('data-need-dump', '[data-dump]')
div.setAttribute(
	'data-is-search-form',
	JSON.stringify({
		tokens: {
			text: {
				title: 'text',
				icon: 'labels',
				operators: [{ value: '含む', default: 'true' }],
				token: 'gl-filtered-search-token',
			},
			user: {
				title: 'user',
				token: {
					'en:is': 'func',
					code: `
							const body = JSON.stringify({
								query: \`query($word: String) {
									users(search: $word, first: 25) {
										nodes { id, title: name, value: name }
									}
								}\`,
								variables: { word }
							})
							return fetch('https://gitlab.com/api/graphql', {
								method: 'post',
								headers: { 'content-type': 'application/json' },
								body,
							})
								.then(r => r.json())
								.then(d => d.data.users.nodes)
						`,
				},
			},
		},
		value: [],
	}),
)
main.appendChild(div)
