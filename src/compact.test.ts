import { escape, unescape } from './compact'

test('es', () => {
	for (const s of [
		'hoge',
		'ho/ge',
		'ho/////ge',
		'ho/;ge',
		'ho;:g=e',
		'h==o!ge',
	])
		expect(unescape(escape(s))).toBe(s)
})

test('es2', () => {
	for (const { s, t } of [
		{ s: 'hoge', t: 'hoge' },
		{ s: 'ho/ge', t: 'ho//ge' },
		{ s: 'ho/;ge', t: 'ho///sge' },
		{ s: 'ho;:g!e', t: 'ho/s:g/ee' },
		{ s: 'h!!oge', t: 'h/e/eoge' },
		{ s: 'h==o!ge', t: 'h==o/ege' },
	])
		expect(escape(s)).toBe(t)
})
