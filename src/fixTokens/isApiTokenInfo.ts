import { ApiTokenInfo } from './interfaces'

export const isApiTokenInfo = (token: unknown): token is ApiTokenInfo =>
	'object' === typeof token &&
	!!token &&
	'en:is' in token &&
	'string' === typeof (token as { 'en:is': unknown })['en:is']
