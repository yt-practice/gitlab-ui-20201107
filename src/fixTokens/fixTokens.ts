/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/ban-ts-comment */

import type { SearchToken, SmallToken } from './interfaces'
import { opsmap } from './opsmap'
import { parseApiToken } from './parseApiToken'

export const fixTokens = (t: Record<string, SmallToken>): SearchToken[] =>
	Object.entries(t).map(([type, value]) => ({
		type,
		operators: opsmap.def,
		icon: 'labels',
		...value,
		token: parseApiToken(value.token || 'gl-filtered-search-token'),
	}))
