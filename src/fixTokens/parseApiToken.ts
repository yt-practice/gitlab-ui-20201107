/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/ban-ts-comment */

import { Component, AsyncComponent } from 'vue'
import EnApiToken from '../tokens/ApiToken.vue'
import { ApiTokenInfo } from './interfaces'
import { isApiTokenInfo } from './isApiTokenInfo'

export const parseApiToken = (
	token: string | ApiTokenInfo | Component | AsyncComponent,
): string | Component | AsyncComponent => {
	if (isApiTokenInfo(token)) {
		switch (token['en:is']) {
			case 'info':
				return {
					...EnApiToken,
					methods: {
						...(EnApiToken as any).methods,
						fetchLabels(word: string) {
							const url = new URL('/', location.href)
							url.pathname = token.pathname
							const { titlekey, resultskey } = token
							if (word) url.searchParams.set(titlekey, word)
							return fetch(String(url))
								.then(r => r.json())
								.then(d => {
									return d[resultskey].map((u: Record<string, string>) => ({
										id: u.id,
										title: u[titlekey],
										value: u[titlekey],
										color: u.color,
										text_color: u.text_color,
									}))
								})
						},
					},
				}
			case 'func':
				return {
					...EnApiToken,
					methods: {
						...(EnApiToken as any).methods,
						fetchLabels: new Function('word', token.code),
					},
				}
			default:
				throw new Error('unknown type')
		}
	}
	return token
}
